# Weather Reporter #

###Description
    Reports weather information for a specified ZIP Code.

###Requirements

The following packages must be installed via Pip.

    selenium (3.9.0)


###Arguments
    -z, --zip
        Display weather information for this ZIP Code.
    -h, --help
        Print this message and exit.

###Examples
    "C:\Program Files (x86)\Python\python.exe" E:/workspace/python/3x/weather_reporter/weather_reporter.py -h
    "C:\Program Files (x86)\Python\python.exe" E:/workspace/python/3x/weather_reporter/weather_reporter.py -z 80014