from data_manager import DataManager
from datetime import date
import logging


def get_month_of_highest_total_rainfall(zip_code):
    """For the given zip code, returns the month with highest total rainfall.

    Args:
        zip_code (str): The target zip code to analyze.

    Returns:
        Dict
    """
    dict_list = DataManager(zip_code).get_zip_data()
    last_index = len(dict_list) - 1

    total_date = None
    total_max_rainfall = 0.0
    curr_month = None
    rainfall_counter = 0.0

    for i, d in enumerate(dict_list):
        curr_date = date(int(d['year']), int(d['month']), int(d['day']))
        rainfall = float(d['precipitation_inches'])

        logging.debug("Checking {0:%Y}-{0:%m}-{0:%d}...".format(curr_date))

        if i == 0:
            curr_month = curr_date

        if curr_date.month == curr_month.month and i != last_index:
            rainfall_counter += rainfall
        else:
            if rainfall_counter >= total_max_rainfall:
                total_max_rainfall = rainfall_counter
                total_date = curr_month
                logging.debug("{0:%Y}-{0:%m} reigns supreme with a new maximum total of {1:0.2f} inches."
                              .format(total_date, total_max_rainfall))
            else:
                logging.debug("{0:%Y}-{0:%m} was rejected with only {1:0.2f} inches."
                              .format(curr_month, rainfall_counter))

            curr_month = curr_date
            rainfall_counter = 0.0

    ret_dict = {'date': total_date, 'total': total_max_rainfall}
    return ret_dict


def get_date_of_highest_rainfall(zip_code):
    """For the given zip code, returns the date of highest total rainfall.

    Args:
        zip_code (str): The target zip code to analyze.

    Returns:
        Dict
    """
    dict_list = DataManager(zip_code).get_zip_data()
    last_index = len(dict_list) - 1

    max_date = None
    max_rainfall = 0.0

    for i, d in enumerate(dict_list):
        curr_date = date(int(d['year']), int(d['month']), int(d['day']))
        rainfall = float(d['precipitation_inches'])

        logging.debug("Checking {0:%Y}-{0:%m}-{0:%d}...".format(curr_date))

        if i == 0:
            max_date = curr_date

        if rainfall >= max_rainfall:
            max_rainfall = rainfall
            max_date = curr_date
            logging.debug("{0:%Y}-{0:%m}-{0:%d} reigns supreme with a new maximum total of {1:0.2f} inches."
                          .format(max_date, max_rainfall))
        else:
            logging.debug("{0:%Y}-{0:%m}-{0:%d} was rejected with only {1:0.2f} inches."
                          .format(curr_date, rainfall))
            if i != last_index:
                logging.debug("Checking {0:%Y}-{0:%m}-{0:%d}...".format(curr_date))

    ret_dict = {'date': max_date, 'total': max_rainfall}
    return ret_dict


def predict_average_highs_and_lows_for_next_year(zip_code):
    """Predicts the average high and low temperatures, per day, for the next year.

    Args:
        zip_code (str): The target zip code to analyze.

    Returns:
        Dict
    """
    dict_list = DataManager(zip_code).get_zip_data()

    date_dict = dict()

    for i, d in enumerate(dict_list):
        curr_date = date(int(d['year']), int(d['month']), int(d['day']))
        key_date = "{0:%m}-{0:%d}".format(curr_date)
        curr_high = float(d['temp_max'])
        curr_low = float(d['temp_min'])

        if key_date not in date_dict:
            logging.debug("Added {0}".format(key_date))
            temp_dict = dict()
            temp_dict['high'] = int(curr_high)
            temp_dict['low'] = int(curr_low)
            date_dict[key_date] = temp_dict
        else:
            hist_high = float(date_dict[key_date]['high'])
            hist_low = float(date_dict[key_date]['low'])

            high_change = ((hist_high + curr_high) / 2) - hist_high
            low_change = ((hist_low + curr_low) / 2) - hist_low

            date_dict[key_date]['high'] = int((hist_high + curr_high) / 2)
            date_dict[key_date]['low'] = int((hist_low + curr_low) / 2)

            logging.debug("{0:%Y}-{0:%m}-{0:%d}".format(curr_date))
            logging.debug("Historical  {0} (H) {1} (L)".format(hist_high, hist_low))
            logging.debug("Current     {0} (H) {1} (L)".format(curr_high, curr_low))
            logging.debug("Change      {0:+0.2f} (H) {1:+0.2f} (L)".format(high_change, low_change))
            logging.debug("New         {0} (H) and {1} (L)"
                          .format(date_dict[key_date]['high'], date_dict[key_date]['low']))
            logging.debug("--------------------------")

    return date_dict


def predict_average_rainfall_by_month(zip_code, target_month):
    """Returns a prediction of total rainfall for the target month

    Args:
        zip_code (str): The target zip code to analyze.
        target_month (int): The target month

    Returns:
        str: A date string, in the format YYYY-MM-DD, with the highest average rainfall.
    """
    dict_list = DataManager(zip_code).get_zip_data()
    last_index = len(dict_list) - 1

    avg_total_rainfall = 0.0
    curr_year = 0
    curr_rainfall = 0.0

    for i, d in enumerate(dict_list):
        curr_date = date(int(d['year']), int(d['month']), int(d['day']))
        rainfall = float(d['precipitation_inches'])

        # logging.debug("{0:%Y}-{0:%m}-{0:%d}: {1:0.2f}".format(curr_date, rainfall))

        if curr_date.year != curr_year or i == last_index:
            logging.debug("{0}-04 Total: {1:0.2f}".format(curr_year, curr_rainfall))
            avg_total_rainfall = (avg_total_rainfall + curr_rainfall) / 2
            curr_year = curr_date.year
            curr_rainfall = 0.0

        if curr_date.month == target_month:
            logging.debug("{0:%Y}-{0:%m}-{0:%d}: {1:0.2f}".format(curr_date, rainfall))
            curr_rainfall += rainfall

    return avg_total_rainfall
