from analysis_lib import *
import sys
import getopt
import logging


def main(argv):
    """Main function

    Args:
        argv:   Command line arguments
    """
    try:
        opts, args = getopt.getopt(argv, "hz:", ["help=", "zip="])
    except getopt.GetoptError as err:
        logging.error("Unable to read argument: " + err.msg)
        print_help()
        sys.exit(2)

    if len(opts) == 0:
        print_help()
    else:
        for opt, arg in opts:
            if opt in ("-z", "--zip"):
                run_report(arg)
            elif opt in ("-h", "--help"):
                print_help()

    logging.debug("Shutting down...")
    sys.exit(0)


def run_report(zip_code):
    """Runs all reports for the targeted ZIP Code.

    Args:
        zip_code (str): The target zip code to analyze.
    """
    logging.info("Checking weather data for ZIP Code {0!s}".format(zip_code))

    res = predict_average_highs_and_lows_for_next_year(zip_code)
    output = "\nNext Year Predicted Highs And Lows\n\n" +\
             "Date   High   Low\n" +\
             "-------------------"
    for date_key in sorted(res.keys()):
        temp_dict = res[date_key]
        output += "\n{0}: {1} (H) {2} (L)".format(date_key, temp_dict['high'], temp_dict['low'])
    logging.info(output)

    res = get_month_of_highest_total_rainfall(zip_code)
    logging.info("Found that {0:%Y}-{0:%m} had the highest total rainfall at {1:0.2f} inches."
                 .format(res['date'], res['total']))

    res = get_date_of_highest_rainfall(zip_code)
    logging.info("Found that {0:%Y}-{0:%m}-{0:%d} had the highest amount of rainfall at {1:0.2f} inches."
                 .format(res['date'], res['total']))

    pred_rain = predict_average_rainfall_by_month(zip_code, 4)
    logging.info("Based on historical data, {0:0.2f} inches are predicted for April 2018.".format(pred_rain))


def print_help():
    """Prints the help/usage message to the console.
    """
    help_str = """
Weather Reporter

Description
    Reports weather information for a specified ZIP Code.

Arguments
    -z, --zip
        Display weather information for this ZIP Code.
    -h, --help
        Print this message and exit.
"""
    print(help_str)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%m-%d-%Y %H:%M:%S',
                        level=logging.INFO)
    logging.debug("Starting up...")
    main(sys.argv[1:])
