import csv
import logging
import os
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait


class DataManager:
    """Singleton data manager responsible for accessing, loading, saving, and scraping weather data.
    """
    class __DataManager:
        DB_DIR = "./zip_data/"

        data_list = None
        zip_code = None

        def __init__(self, target_zip):
            self.zip_code = target_zip
            logging.debug("CSV Manager is awake.")

        def get_zip_data(self):
            """Gets data for the target ZIP Code.

            Returns:
                List
            """
            if not self.data_list:
                self.load_csv()
            return self.data_list

        def load_csv(self):
            """Loads ZIP Code data from a csv file.
            """
            data_dict_list = list()
            count = 0

            if not os.path.isfile(os.getcwd() + "/zip_data/{0}.csv".format(self.zip_code)):
                logging.info("No data found for {0}.  Starting data scrape to collect some.".format(self.zip_code))
                self.scrape_weather_data_for_years()

            with open(self.DB_DIR + self.zip_code + ".csv", newline='') as file:
                reader = csv.DictReader(file)
                for row in reader:
                    data_dict_list.append(row)
                    count += 1

            logging.debug("Loaded {0} days of data for Zip {1}".format(count, self.zip_code))
            self.data_list = data_dict_list

        def save_csv(self, data_list):
            """Saves ZIP Code data to a new csv file.
            """
            with open(self.DB_DIR + self.zip_code + ".csv", 'w', newline='') as file:
                fieldnames = ['year', 'month', 'day', 'temp_max', 'temp_min', 'precipitation_inches']
                writer = csv.DictWriter(file, fieldnames=fieldnames)
                writer.writeheader()
                for d in data_list:
                    writer.writerow(d)

        def scrape_weather_data(self, target_year):
            """Using Selenium, scrapes data for the target year from wunderground.com's daily history.

            Defaults to Chrome, with Firefox as a fallback.

            Args:
                target_year (str):

            Returns:
                List
            """
            driver_lib_path = os.getcwd() + "/external_exe/chromedriver_2.35/chromedriver.exe"
            driver = webdriver.Chrome(executable_path=driver_lib_path)
            if not driver:
                driver_lib_path = os.getcwd() + "/external_exe/geckodriver-0.19.1-win64/geckodriver.exe"
                driver = webdriver.Firefox(executable_path=driver_lib_path)

            logging.info("Preparing to scrape data for {0}".format(target_year))

            # 1 Load page
            url = "https://www.wunderground.com/history/airport//2018/02/21/DailyHistory.html"
            driver.get(url)
            driver.wait = WebDriverWait(driver, 15)
            driver.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[value*='Submit']")))

            # 2 Set Search
            location_txtbx = driver.find_element_by_css_selector("input#histSearch")
            location_txtbx.send_keys(self.zip_code)

            month_select = driver.find_element_by_css_selector("select.month")
            month_select = Select(month_select)
            month_select.select_by_visible_text("January")

            day_select = driver.find_element_by_css_selector("select.day")
            day_select = Select(day_select)
            day_select.select_by_visible_text("1")

            year_select = driver.find_element_by_css_selector("select.year")
            year_select = Select(year_select)
            year_select.select_by_visible_text(target_year)

            submit_btn = driver.find_element_by_css_selector("input[value*='Submit']")
            submit_btn.click()

            # 3 Update Search to use custom range

            custom_tab = driver.find_element_by_link_text("Custom")
            custom_tab.click()

            driver.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "div.custom-date-select>div:nth-of-type(2) select.month")))

            # 4 Select To for custom range and search
            to_month_select = driver.find_element_by_css_selector("div.custom-date-select>div:nth-of-type(2) select.month")
            to_month_select = Select(to_month_select)
            to_month_select.select_by_visible_text("December")

            to_day_select = driver.find_element_by_css_selector("div.custom-date-select>div:nth-of-type(2) select.day")
            to_day_select = Select(to_day_select)
            to_day_select.select_by_visible_text("31")

            to_year_select = driver.find_element_by_css_selector("div.custom-date-select>div:nth-of-type(2) select.year")
            to_year_select = Select(to_year_select)
            to_year_select.select_by_visible_text(target_year)

            get_hist_btn = driver.find_element_by_css_selector("input[value='Get History']")
            get_hist_btn.click()

            driver.wait.until(EC.presence_of_element_located((
                By.CSS_SELECTOR, "div.custom-date-select>div:nth-of-type(2) select.month")))

            # 5 Page Loaded, scrape
            sel_str = "//div[@id='observations_details']//tbody[not(.//td[@class='daily-td-grey'])]/tr"
            driver.wait.until(EC.visibility_of_all_elements_located((By.XPATH, sel_str)))
            rows = driver.find_elements_by_xpath(sel_str)

            logging.info("Starting data scrape.  Hang on, this could take awhile...".format(target_year))

            month = 0
            ret_list = list()
            for row in rows:
                day = row.find_element_by_css_selector("td:nth-of-type(1)").text
                temp_max = row.find_element_by_css_selector("td:nth-of-type(2)").text
                if temp_max == '-':
                    temp_max = ret_list[len(ret_list) - 1]['temp_max']
                temp_min = row.find_element_by_css_selector("td:nth-of-type(4)").text
                if temp_min == '-':
                    temp_min = ret_list[len(ret_list) - 1]['temp_min']
                precip = row.find_element_by_css_selector("td:nth-of-type(20)").text
                if precip == 'T':
                    precip = "0.00"

                if day == "1":
                    month += 1

                logging.debug("{0}-{1}-{2} x{3} n{4} p{5}".format(target_year, month, day, temp_max, temp_min, precip))

                temp_dict = dict()
                temp_dict['year'] = target_year
                temp_dict['month'] = month
                temp_dict['day'] = day
                temp_dict['temp_max'] = temp_max
                temp_dict['temp_min'] = temp_min
                temp_dict['precipitation_inches'] = precip

                ret_list.append(temp_dict)

            logging.info("Scraping complete for {0}.".format(target_year))
            driver.quit()
            return ret_list

        def scrape_weather_data_for_years(self):
            """Organizes scrape_weather_data calls to scrape weather data for each of the last four years.
            """
            all_data = list()
            for y in ['2015', '2016', '2017', '2018']:
                all_data.extend(self.scrape_weather_data(y))

            self.save_csv(all_data)

    instance = None

    def __init__(self, target_zip):
        if not DataManager.instance:
            DataManager.instance = DataManager.__DataManager(target_zip)

    def __getattr__(self, item):
        return getattr(self.instance, item)
